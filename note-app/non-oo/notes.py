import os
from subcategory import subcategory_exists

project_path = "/tmp/notes/"

def create_note(category_name, subcategory_name, name, content):
    note_path = project_path+category_name+"/"+subcategory_name
    if subcategory_exists(note_path):
        # create a file name.dat in the given subcategory folder
        f = open(note_path+"/"+name+".dat","w+")
        f.write(content)
        f.close()
        return True
    else:
        return False
def list_notes(project_path,category_name, subcategory_name):
    if subcategory_exists(project_path,category_name,subcategory_name):
        return os.listdir(project_path+category_name+"/"+subcategory_name)
    else:
        return []

def read_note(project_path, category_name, subcategory_name, name):
    if subcategory_exists(project_path, category_name, subcategory_name):
        f = open(note_path+"/"+name+".dat","r")
        content = f.readlines()
        f.close()
        return content
def delete_note(project_path, category_name, subcategory_name, name):
    if subcategory_exists(project_path, category_name, subcategory_name):
        os.remove(project_path+category_name+"/"+subcategory_name+"/"+name+".dat")

