import os

project_path = "/tmp/notes/"
def create_category(category_name):
    if os.path.isdir(category_name):
        return True
    else:
        os.mkdir(project_path+category_name)
        return True

def delete_category(category_name):
    if os.path.isdir(project_path+category_name):
        for name in os.listdir(project_path+category_name):
            os.rmdir(project_path+category_name+"/"+name)
        os.rmdir(project_path+category_name)
        return True

def category_exists(category_name):
    if os.path.isdir(project_path+category_name):
        return True
    else:
        return False

def list_categories():
    return os.listdir(project_path)

#print list_categories()
#print create_category("hello")
#print list_categories()
#print delete_category("hello")
#tlab.com:voidspacexyz/python-examplesprint list_categories()
