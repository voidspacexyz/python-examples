import os
from category import category_exists

project_path = "/tmp/notes/"

def create_sub_category(category_name, subcategory_name):
    if category_exists(category_name):
        os.mkdir(project_path+category_name+"/"+subcategory_name)
        return True
    else:
        return False

def delete_sub_category(category_name,subcategory_name):
    if category_exists(category_name):
        os.rmdir(project_path+category_name+"/"+subcategory_name)
        return True
    else:
        return False

def subcategory_exists(category_name,subcategory_name):
    if os.path.isdir(project_path+category_name+"/"+subcategory_name):
        return True
    else:
        return False


def list_subcategories(category_name):
    if category_exists(category_name):
        return os.listdir(project_path+category_name)
    else:
        return False
#print list_subcategories("hello/")
#print create_sub_category("hello/","test1")
#print create_sub_category("hello/","test2")
#print list_subcategories("hello/")
#print delete_sub_category("hello/","test1")
#print list_subcategories("hello/")
