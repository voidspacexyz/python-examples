## Python Code Examples 


 What is it ?
 ------------

 	This project consists of various examples that help people learning python to refer to various code. It also serves the purpose of speakers distributing examples for participants during any python introduction workshops.

 Installation ?
 --------------

  	Unless explictly mentioned, all you would need is python installed. For now all the examples are written in Python2. We will soon write Python3 code too. But, each project (every folder/file is a project here), would contain its own README, which explains what the project is about, and any 3rd party library that is required for the project to run

  	As a best practice, it is always recommened and safe, to run all the code in a VirtualEnvironment, and is also considered the best practice to do so. The steps to install virtualenv is mentioned here : 

	* First install Python-PIP :  https://pip.pypa.io/en/stable/installing/

	* Installing virtualenv : https://virtualenv.pypa.io/en/stable/installation/

 Project Structure ?
 -------------------

	* Since this project is aimed to help beginners, we follow a simple project structure. Each project is organized as a folder.
	* Each folder contains a README (Explaining in detail about the project), the code, and any database schema (if needed).
	* Each code, is advised to contain both Class based working code and a non-class based working code.
	* Also for projects like "Grocery list", it also adivsed to write both class and non-class based code also with a file backend and a database backend.

 Issues ?
 --------

	If you find a bug or want a new code, please do feel free to raise an issue here : 

 Contribution Guide ?
 --------------------

	If you are a programmer and see value in this project, you are more than welcome to contribute, we try to keep things pretty simple. We follow the 2 Python PEP conventions, PEP-8 (Code) and PEP-257 (Documentation).
	
	In short, try to stick to this :  https://gist.github.com/sloria/7001839

	Also to all those lazy people, there exists a autopep8 package in pip, that will do a lot of these stuff. But remember it cant write code or documentation by itself. 

	To start with contributing, look at the list of issues, see what you can fix, mention a comment saying "I am working on it" and share constant progress on your updates. Send a Merge Request, and lets review and make sure the code goes in.

	A side note : For a bug that has not progress for a long time, you are free to say, "I am willing to take over this bug", wait for sometime, and if no response from the original bug squasher, you are free to catch and squash the bug.

	Incase of a new project idea to come in, it is highly advisable to to raise a new issue with the  "New Idea" tag, and lets debate over it.

	Also make sure to add your name to the  contributors file.
 Licensing ?
 ---------

  Please see the file called LICENSE.

 Contacts
 --------

	The following emails can for now be used incase the issues is not helping you.
	* null@voidspace.xyz
	* Abhinav, Tanvi, Adithya add your email id's 
